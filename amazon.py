import requests
import re
from bs4 import BeautifulSoup
from selenium import webdriver
import time
from selenium.webdriver.chrome.options import Options

options = Options()
options.add_argument('--headless')
options.add_argument('--disable-gpu')
driver = webdriver.Chrome('C:\chromedriver_win32\chromedriver.exe',chrome_options=options)

def getItemList(html):
    re0 = ';asin=B01N9Y87LS&amp;'
    #print(str(BeautifulSoup(html, "html.parser")))

    elem = (str(BeautifulSoup(html, "html.parser"))).split(';asin=')[1:]
    asinList = []
    for e in elem:
        #print(e)
        if e.find('&amp') != -1:
            asinList.append(e[:e.find('&amp')])
    #pattern = re.compile(r";asin=(\w)&amp;")
    #asinList = (re.findall(pattern, str(BeautifulSoup(html, "html.parser"))))
    return asinList, len(asinList)


search_keyword = 'laptop'
url = 'http://www.amazon.com/s?keywords='+search_keyword
#htmlText = (requests.get(url))
#soup = BeautifulSoup(htmlText.text, "html.parser")
#print(htmlText.status_code)
#print(soup)


driver.get(url)
time.sleep(2)
print(getItemList(driver.page_source))
#print(getItemList(str(htmlText.content)))
driver.close()