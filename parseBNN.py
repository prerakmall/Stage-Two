from bs4 import BeautifulSoup
import os
import glob
import codecs
import csv


XML_FILE_EXTENSION = '*.html'
CSV_FILE_EXTENSION = '.csv'
WEBDATA_RELATIVE_PREFIX = 'data/srcTwo/'
CSV_OUTPUT_FILE = 'bnn' + CSV_FILE_EXTENSION
REPLACE_COMMA = '#'


Title = "Title"
Author = "Author"
Type = "Type"
Pages = "Pages"
Publisher = "Publisher"
ISBN_13 = "ISBN-13"
Age_Range = "Age Range"
Product_Dimension = "Product Dimensions"
Price = "Price"

OUTPUT_FORMAT = [Title, Author, Pages, Publisher, ISBN_13, Age_Range, Product_Dimension, Price]
def generate_xml_files():
    path = get_file_path(WEBDATA_RELATIVE_PREFIX) + XML_FILE_EXTENSION
    files = glob.glob(path)
    mapList = []
    #count = 0
    for file_name in files:
        d = {}
        f = codecs.open(file_name, 'r', encoding="utf-8")
        xml_content = f.read()
        soup = BeautifulSoup(xml_content, 'html.parser')
        get_attributes(soup, d) # get data from table
        mapList.append(d)
        #print(d.keys())
        #count += 1
        #if count == 10:
        #    break

    keys = []
    bookList = []
    for m in mapList:
        for k in m.keys():
            if k not in keys:
                keys.append(k)

    for m in mapList:
        book = []
        for k in keys:
            if k in m.keys():
                book.append(m[k].replace(',', REPLACE_COMMA))
            else:
                book.append('')

        bookList.append(book)

    #print(bookList)
    #print(keys)
    write_to_file(bookList, keys)

def print_map(map):
    for k, v in map.items():
        print (k + " ---> " + v)

def write_to_file(book_list, keys):
    create_file_path = get_file_path(CSV_OUTPUT_FILE)

    columns = [keys]
    with open(create_file_path, 'w', newline='') as to_write_file:
        writer = csv.writer(to_write_file)
        writer.writerow(keys)
        for book in book_list:
            writer.writerow(book)

def findnth(haystack, needle, n):
    parts = haystack.split(needle, n+1)
    if len(parts)<n+1:
        return -1
    return len(haystack)-len(parts[-1])-len(needle)

def get_attributes(soup, d):
    details = soup.find('div',id="ProductDetailsTab")
    #print(details)
    title = soup.find('div', id='pdp-header-info')
    author = soup.find('span', id='key-contributors')
    price = soup.find('span', id='pdp-cur-price')
    reviews = soup.find('span', attr={'class':'gig-average-review'})
    rows = details.findAll('td')
    #for r in rows:
    #    print(r.get_text().rstrip().lstrip())

    rowHeads = details.findAll('th')
    for i in range(len(rowHeads)):
        if rowHeads[i].get_text().rstrip().lstrip()[:-1] =='Lexile':
            d[rowHeads[i].get_text().rstrip().lstrip()[:-1]] = rows[i].get_text().rstrip().lstrip().split()[0]
        else:
            d[rowHeads[i].get_text().rstrip().lstrip()[:-1]] = rows[i].get_text().rstrip().lstrip()

    author = author.get_text().rstrip().lstrip().replace('by','').replace('\n','')
    i = findnth(author, author.split()[0], 2)
    #print(author.split(author.split()[0]))
    #print(author[:i])
    #print(title.get_text().rstrip().lstrip().split('by')[0].replace('\n',''))


    d['Title'] = title.get_text().rstrip().lstrip().split('by')[0].replace('\n','')
    d['Author'] = author[1:i]
    if price == None:
        price = soup.find('span', id='pdp-cur-price-BuyNew')
        if price == None:
            d['Price'] = '-1'
        else:
            d['Price'] = price.get_text()
    else:
        d['Price'] = price.get_text()
    if reviews == None:
        d['Average Review'] = '-1'
    else:
        d['Average Review'] = reviews.get_text()

    #print(d)
    return d

def get_file_path(file_name):
    current_file_path = os.path.dirname(__file__)
    return os.path.join(current_file_path, file_name)


generate_xml_files()